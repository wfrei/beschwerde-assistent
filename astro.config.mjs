import { defineConfig } from 'astro/config';
import astroI18next from "astro-i18next";

// https://astro.build/config
import image from "@astrojs/image";

// https://astro.build/config
import preact from "@astrojs/preact";

// https://astro.build/config
export default defineConfig({
  integrations: [astroI18next(), image({
    serviceEntryPoint: '@astrojs/image/sharp'
  }), preact()]
});