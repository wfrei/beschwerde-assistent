---
name: Lichtverschmutzung
placeholders:
    your_name:
        type: text
        placeholder: Dein Name
    your_residence:
        type: text
        placeholder: Dein Wohnort
    manufacturer:
        type: custom:manufacturer
        placeholder: Dein Wohnort
---
Guten Tag,

ich bin {{your_name}} und ich wohne in {{your_residence}}.
In der Nähe meiner Wohnung befindet sich eine digitale Werbeanlage von {{manufacturer}}.
Das Licht, was diese Anlage produziert stört meiner Meinung nach. Es hält vom Schlafen ab und kann Verkehrsteilnehmer blenden.

Mit freundlichen Grüßen,
{{your_name}}