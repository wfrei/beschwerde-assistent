import { MarkdownInstance } from "astro";

export const filterTemplates = (templates: MarkdownInstance<TemplateFrontmatter>[], match_language: string) => {
    templates = templates.filter((template) => {
        const file = template.file.split("/");
        file.pop();
        const fileLang = file.pop();

        return match_language == fileLang;
    })

    return templates
}

export const extractTemplateName = (template: MarkdownInstance<TemplateFrontmatter>) => {
    const name = template.file.split("/").pop().split(".").shift()

    return name
}

export type TemplatePlaceholder = {
    type: string
    placeholder: string
}

export type TemplatePlaceholderList = Record<string, TemplatePlaceholder>

export type TemplateFrontmatter = {
    name: string
    placeholders: TemplatePlaceholderList
}