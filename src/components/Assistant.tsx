import { TemplatePlaceholderList } from "../utilities/templates"


const Assistant = (props: {placeholders: TemplatePlaceholderList, rawContent: string}) => {
    return <>
        <pre>{JSON.stringify({placeholders: props.placeholders, rawContent: props.rawContent}, null, 2)}</pre>
    </>
}

export default Assistant