/** @type {import('astro-i18next').AstroI18nextConfig} */
export default {
    defaultLanguage: "de",
    supportedLanguages: ["de"],
    i18next: {
      debug: true,
      initImmediate: false,
      ns: ["common"],
      defaultNS: "common",
      backend: {
        loadPath: "./src/locales/{{lng}}/{{ns}}.json",
      },
    },
    i18nextPlugins: { fsBackend: "i18next-fs-backend" },
  };